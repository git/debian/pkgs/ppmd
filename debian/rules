#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DESTDIR := $(CURDIR)/debian/ppmd

DEB_HOST_ARCH_CPU ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_CPU)
DEB_HOST_ARCH_BITS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)

CXXFLAGS = -Wall -g

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
  CXXFLAGS += -O0
else
  # FIXME: Use this workaround for now, otherwise with g++ >= 4.1 the code
  #        segfaults, thus FTBFS. Upstream uses -O1 on optimized builds.
  ifneq (,$(filter $(DEB_HOST_ARCH_CPU),powerpc sparc))
    CXXFLAGS += -O1
  else
    CXXFLAGS += -O2
  endif
endif

# Define architecture type depending on the word size, and lax (NORMAL) or
# strict (EXOTIC) alginment rules. Always default to strict alignment, as
# it's the safest choice, although it might consume more memory.
ifeq (32,$(DEB_HOST_ARCH_BITS))
  ifneq (,$(filter $(DEB_HOST_ARCH_CPU),i386 avr32 m68k powerpc s390))
    CXXFLAGS += -D_32_NORMAL
  else
    CXXFLAGS += -D_32_EXOTIC
  endif
else
  ifneq (,$(filter $(DEB_HOST_ARCH_CPU),amd64 s390))
    CXXFLAGS += -D_64_NORMAL
  else
    CXXFLAGS += -D_64_EXOTIC
  endif
endif

build-indep:

build-arch:
	dh_testdir
	
	$(MAKE) CXXFLAGS="$(CXXFLAGS)"

build: build-indep build-arch

clean:
	dh_testdir
	dh_testroot
	
	[ ! -f Makefile ] || $(MAKE) clean
	
	dh_clean

install: check
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	
	$(MAKE) install DESTDIR=$(DESTDIR) prefix=/usr

check: build
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	@echo "Testing if ppmd works on this architecture"
	$(MAKE) test
endif

.PHONY: build-indep build-arch build install check clean

binary-indep:
# Nothing to do.

binary-arch: install
	dh_testdir
	dh_testroot
	dh_installdocs
	dh_installexamples
	dh_installman
	dh_installchangelogs
	dh_install
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch

.PHONY: binary-indep binary-arch binary
